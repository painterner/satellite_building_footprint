import tensorflow as tf
import keras 
import keras.backend as K
import keras.engine as KE
import keras.layers as KL
import keras.models as KM
import numpy as np
from  .utils import batch_slice

# def calculate_loss(inputs):
#     [mask,line] = inputs[0]
#     gt_mask = inputs[1]

#     ## 1. circle0 - (circle1 + circle2 + ... + circlen)
#     ## line shape: (5,10*2+1)  
#     #  10 line ( need skew ratio and bias), 
#     #  1 threshold, if no reach threshold, the Occlusion area is not considered

#     splited_ = tf.split(line,(10*2,1),axis=1)
#     scores = splited_[1]
#     line = splited_[0]
#     ix = tf.where(scores > 0.9)  ## if there is tf.threshold
#     ix = tf.squeeze(ix,axis=1)
#     line = tf.gather_nd(line,ix)

#     line_splited = tf.split(line,10,axis=1)


# def select_and_loss_class(inputs,func,config):
#     net = inputs[0]
#     classes_count = func(net)

#     ## classes_count has shape: [classes, Occulsion]
#     classes_count = tf.minium(config.PTNER_CLASSES_COUNT,classes_count)
#     net = tf.gather_nd(net,classes_count)

#     ## class_count loss
#     loss = None
#     return net, classes_count, loss

def translation_mask(mask,offset):
    def execute(mask_slice,offset_slice):
        """ mask_slice: [w,h] offset_slice: [2]"""
        w,h = tf.shape(mask_slice)

        p1,p2 = tf.split(offset,2)

        right_idx = tf.where(p1 > 0)
        left_idx = tf.where(p1 < 0)
        right = tf.gather_nd(p1,right_idx)
        left = tf.gather_nd(p1,left_idx)

        down_idx = tf.where(p2 > 0)
        top_idx = tf.where(p2 < 0)
        down = tf.gather_nd(p2,down_idx)
        top = tf.gather_nd(p2,top_idx)

        leftpadding = right
        rightpadding = left
        toppadding = down
        downpadding = top
        mask_slice = tf.pad(mask_slice,[leftpadding,rightpadding],[toppadding,downpadding])

        denominator_w = (w+rightpadding+leftpadding-1)
        denominator_h = (h+toppadding+downpadding-1)
        cropbox = ( rightpadding/denominator_w , downpadding/denominator_h,(w-1)/denominator_w, (h-1)/denominator_h )
        mask_slice = tf.image.crop_and_resize(mask_slice,cropbox,box_ind=tf.constant([0]),crop_size=(w,h),name='batch_slice_translation_crop_and_resize')
        
    mask = batch_slice([mask,offset], lambda x,y: execute(x,y), tf.shape(mask)[0], names="batch_slice_translation_mask")

    return mask

def Mask_Integral(mask,gt_mask):
    """ is not good, if 1024*1024 length add, need 1024*1024*(1024*1024+1)/2 *1024 about to 512TB """
    Imask = tf.range(0,1024*1024)
    weights = tf.range(0,1024*1024)
    Igt_mask = tf.range(0,1024*1024) * weights
    return Imask,Igt_mask

def cosine(mask,gt_mask):
    a = tf.reshape(mask,(tf.shape(mask)[0],-1) )
    b = tf.reshape(gt_mask,(tf.shape(gt_mask)[0],-1))
    r = tf.reduce_sum(a*b,-1) / (tf.sqrt(tf.reduce_sum(a**2,-1))*tf.sqrt(tf.reduce_sum(b**2,-1)))
    return r

def mask_net(inputs):
    shape = tf.shape(inputs)
    x = KL.Deconv2D(shape[3]/2,(3,3),strides=(2,2),name="mask_net_conv0")(inputs)
    x = KL.Deconv2D(shape[3]/4,(3,3),strides=(2,2),name="mask_net_conv1")(x)
    x = KL.Dense(1,input_shape=(tf.shape(x)[-1]))(x)
    x = tf.squeeze(x,-1)
    return x
def offset_net(inputs):
    shape = tf.shape(inputs)
    x = KL.Conv2D(1,(3,3),name='offset_conv')(inputs)
    x = tf.reshape(x,(shape[0],-1))
    x = KL.Dense(2,input_shape=(shape[-1],))(x)
    return x

def loss_mask(inputs):
    """shape: (2,256,256,256)"""
    net = inputs[0]
    gt_mask = inputs[1]

    mask = mask_net(net)
    offset = offset_net(net)

    # gt_mask translation transformation
    mask = translation_mask(mask,offset)
    # should we use pixelwise loss or cosine loss  or Integral loss
    # Imask, Igt_mask = Mask_Integral(mask,gt_mask)
    Imask, Igt_mask = cosine(mask,gt_mask)
    loss = tf.reduce_sum(Imask**2 - Igt_mask**2) / (1024*1024)
    return net,mask,loss

def predict_mask(inputs):
    assert inputs is (tuple,list)
    net = inputs[0]
    mask = mask_net(net)
    offset = offset_net(net)
    mask = translation_mask(mask,offset)
    return mask

## Can we use cLT.polygonize (definited in CosmiQ_SN4_Baseline/bin/make_predictions.py) to get polyshape
# def search_polygons(inputs,func,config):
#     """
#          __________            _____
#         |     . .  |          / . . \
#         |   ...... |         /...... \
#         |  ....... |  ===>  |........|
#         |   .....  |        \ ..... /
#         |__________|         \_____/
#     """
#     boxes,scores = inputs[0]
#     gt_polygons=inputs[1]
#     ## gt_polygons zero padding to config.PTNER_CLASSES_COUNT
#     ## gt_polygons normalize

#     polygons = tf.gather_nd(boxes,scores)
#     ##poly
#     polygons,polygons_scores = func(boxes) 
#     polygons_mask = tf.where(polygons_scores > 0.9)
#     polygons = polygons * polygons_mask
#     ## problem: Does net know what's the suitable length of every eage ?
#     ## dircetly loss boxes and 
#     loss_polygons_mask = None
#     loss_polygons = None
#     loss = loss_polygons_mask + loss_polygons

#     return polygons, loss

def metrics(inputs):

    ## if count > threshold, calculate every length of eage line, drop out short line
    pass


"""
idea:
    all select put in to network, because it's complicated.

"""
    
