import tensorflow as tf
import keras 
import keras.backend as K
import keras.engine as KE
import keras.layers as KL
import keras.models as KM
import numpy as np
import os
import argparse
import warnings

from subworkpath.loss import loss_mask
from subworkpath.model import Model
from subworkpath.utils import get_files_recursively
from subworkpath.DataGenerator import DataGenerator, FlatDataGenerator, FileDataGenerator
import subworkpath as space_base

data_format = 'files'

class Train():
    def __init__(self):
        pass

    def __call__(self,dataset, model='resnet50', data_path='', mask_path='',
         recursive=False, output_path='model.hdf5', tb_dir='',
         data_format='array'):
        # create a few variables needed later.
        output_dir, model_name = os.path.split(output_path)
        tmp_model_path = output_path.rstrip('.hdf5') + '_ckpt_best.hdf5'
        tmp_weights_path = os.path.join(output_dir, 'tmp_weights.h5')
        if not mask_path:
            mask_path = data_path

        # make sure everything is clean to start:
        if os.path.exists(tmp_model_path):
            warnings.warn('Temp file {} existed before starting. Deleted.'.format(
                tmp_model_path))
            os.remove(tmp_model_path)
        if os.path.exists(tmp_weights_path):
            warnings.warn('Temp file {} existed before starting. Deleted.'.format(
                tmp_weights_path))
            os.remove(tmp_weights_path)

        # specify the path to the training and validation data files
        train_im_path = os.path.join(data_path, 'train',
                                    dataset + '_train_ims.npy')
        val_im_path = os.path.join(data_path, 'validate',
                                dataset + '_val_ims.npy')
        train_mask_path = os.path.join(mask_path, 'train',
                                    dataset + '_train_masks.npy')
        val_mask_path = os.path.join(mask_path, 'validate',
                                    dataset + '_val_masks.npy')

        batch_size = 1
        model_args = {
            'optimizer': 'Nadam',
            'input_shape': (512, 512, 3),
            'base_depth': 64,
            'lr': 0.0002
        }
        # # reduce base_depth to 32 if using vanilla unet
        # if model == 'unet':
        #     model_args['base_depth'] = 32

        if data_format == 'array':
            # load in data. don't read entirely into memory - too big.
            train_im_arr = np.load(train_im_path, mmap_mode='r')
            val_im_arr = np.load(val_im_path, mmap_mode='r')
            train_mask_arr = np.load(train_mask_path, mmap_mode='r')
            val_mask_arr = np.load(val_mask_path, mmap_mode='r')

            # create generators for training and validation
            training_gen = FlatDataGenerator(
                train_im_arr, train_mask_arr, batch_size=batch_size, crop=True,
                output_x=model_args['input_shape'][1],
                output_y=model_args['input_shape'][0],
                flip_x=True, flip_y=True, rotate=True
                )
            validation_gen = FlatDataGenerator(
                val_im_arr, val_mask_arr, batch_size=batch_size, crop=True,
                output_x=model_args['input_shape'][1],
                output_y=model_args['input_shape'][0]
                )
            n_train_ims = train_im_arr.shape[0]
            n_val_ims = val_im_arr.shape[0]
        elif data_format == 'files':
            unique_chips = [f.lstrip('mask_').rstrip('.tif')
                            for f in os.listdir(mask_path) if f.endswith('.tif')]
            np.random.shuffle(unique_chips)
            number_train_chips = int(len(unique_chips)*0.8)
            train_chips = unique_chips[:number_train_chips]
            val_chips = unique_chips[number_train_chips:]
            im_fnames = get_files_recursively(data_path,
                                            traverse_subdirs=recursive)
            if dataset != 'all':
                if dataset == 'nadir':
                    collect_subset = space_base.COLLECTS[0:11]
                elif dataset == 'offnadir':
                    collect_subset = space_base.COLLECTS[11:18]
                elif dataset == 'faroffnadir':
                    collect_subset = space_base.COLLECTS[18:]
                im_fnames = [f for f in im_fnames if
                            any(c in f for c in collect_subset)]
                print(collect_subset)
            n_ims = len(im_fnames)
            print('n_ims: {}'.format(n_ims))
            n_train_ims = np.floor(n_ims*0.8)
            n_val_ims = np.floor(n_ims*0.2)

            training_gen = FileDataGenerator(
                im_fnames, mask_path, (900, 900, 3), chip_subset=train_chips,
                batch_size=batch_size, crop=True, traverse_subdirs=recursive,
                output_x=model_args['input_shape'][1],
                output_y=model_args['input_shape'][0],
                flip_x=True, flip_y=True, rotate=True)
            validation_gen = FileDataGenerator(
                im_fnames, mask_path, (900, 900, 3), chip_subset=val_chips,
                batch_size=batch_size, crop=True, traverse_subdirs=recursive,
                output_x=model_args['input_shape'][1],
                output_y=model_args['input_shape'][0])
        print()
        print("<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>")
        print("                 BEGINNING MODEL TRAINING")
        print("                 MODEL ARCHITECTURE: {}".format(model))
        print("                   OPTIMIZER: {}".format(model_args['optimizer']))
        print("                     DATASET: {}".format(dataset))
        print("                 INPUT SHAPE: {}".format(model_args['input_shape']))
        print("                      BATCH SIZE: {}".format(batch_size))
        print("                   LEARNING RATE: {}".format(model_args['lr']))
        print("<><><><><><><><><><><><><><><><><><><><><><><><><><><><><><><>")
        print()

        self.training_gen = training_gen
        self.validation = validation_gen

        # callbax = []
        # callbax.append(ReduceLROnPlateau(factor=0.2, patience=3, verbose=1,
        #                                 min_delta=0.01))
        # callbax.append(ModelCheckpoint(tmp_model_path, monitor=monitor,
        #                             save_best_only=True))
        # callbax.append(TerminateOnMetricNaN('precision'))
        # callbax.append(EarlyStopping(monitor=monitor,
        #                             patience=early_stopping_patience,
        #                             mode='auto'))
        # if tb_dir:  # if saving tensorboard logs
        #     callbax.append(TensorBoard(
        #         log_dir=os.path.join(tb_dir, model_name)))

        # model = Model()
        model = model.build()

        # model.fit_generator(
        #     training_gen, validation_data=validation_gen,
        #     validation_steps=np.floor(n_val_ims/batch_size),
        #     steps_per_epoch=np.floor(n_train_ims/batch_size),
        #     epochs=1000, callbacks=callbax
        #     )
        model.fit_generator(
            training_gen, validation_data=validation_gen,
            validation_steps=np.floor(n_val_ims/batch_size),
            steps_per_epoch=np.floor(n_train_ims/batch_size),
            epochs=1000
            )
        model.save(output_path)

    def get_data_by_index(self,index):
        data = self.training_gen[index]
        return data


train = Train()
train(dataset = '../../dataset')